﻿--cadastro de Veterinários
insert into veterinario (id, nome, ativo) values (1, 'Doutor(a) 1', 't');
insert into veterinario (id, nome, ativo) values (2, 'Doutor(a) 2', 't');
insert into veterinario (id, nome, ativo) values (3, 'Doutor(a) 3', 't');
insert into veterinario (id, nome, ativo) values (4, 'Doutor(a) 4', 't');
insert into veterinario (id, nome, ativo) values (5, 'Doutor(a) 5', 't');
insert into veterinario (id, nome, ativo) values (6, 'Doutor(a) 6', 't');
insert into veterinario (id, nome, ativo) values (7, 'Doutor(a) 7', 'f');
insert into veterinario (id, nome, ativo) values (8, 'Doutor(a) 8', 'f');

--cadastro de Grupos de Usuarios
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (1, 'Administradores', 'Grupo dos Administradores do Sistema','ADMINISTRADOR');
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (2, 'Assistentes de Atendimento', 'Grupo dos Assistentes de Atendimento','ASSISTENTE_ATENDIMENTO');
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (3, 'Assistentes de Farmácia', 'Grupo dos Assistentes de Atendimento','ASSISTENTE_FARMACIA');
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (4, 'Veterinários', 'Grupo dos Veterinários','VETERINARIO');
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (5, 'Gestores', 'Grupo dos Gestores','GESTOR');

--cadastro de Usuarios
insert into usuario (id, nome, email, senha, grupo_id) values (1, 'admin', 'admin@cliente.com.br', '123', 1); 
insert into usuario (id, nome, email, senha, grupo_id) values (3, 'Atendimento Clínico', 'clinico@cliente.com.br', '123', 2); 
insert into usuario (id, nome, email, senha, grupo_id) values (4, 'Atendimento Cirúrgico', 'cirurgico@cliente.com.br', '123', 2); 
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (5, 'Veterinario 1', 'veterinario1@cliente.com.br', '123', 4, 1); 
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (6, 'Veterinario 2', 'veterinario2@cliente.com.br', '123', 4, 2); 
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (7, 'Veterinario 3', 'veterinario3@cliente.com.br', '123', 4, 3);
insert into usuario (id, nome, email, senha, grupo_id) values (8, 'Gestor 1', 'gestor1@cliente.com.br', '123',5);
insert into usuario (id, nome, email, senha, grupo_id) values (9, 'Gestor 2', 'gestor2@cliente.com.br', '123',5);

--Cadastro de Espécies
insert into especie values(1,'Caninos');
insert into especie values(2,'Felinos');
insert into especie values(3,'Lagomorfos (Coelhos)');
insert into especie values(4,'Roedores');
insert into especie values(5,'Psitacídeos (Ordem de Aves)');
insert into especie values(6,'Bovinos');
insert into especie values(7,'Caprinos');
insert into especie values(8,'Equinos');
insert into especie values(9,'Ovinos');
insert into especie values(10,'Suinos');
insert into especie values(11,'Outros (Sem Cadastro)');
--SAVEPOINT especies_salvas;

--Cadastro de Raças
--Outras Raças para cada espécie já que há dependência na hora do cadastro
insert into raca (id, nome, especie_id, tipo_porte) values (1, 'Outros (Sem Cadastro)', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (2, 'Outros (Sem Cadastro)', 2, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (3, 'Outros (Sem Cadastro)', 3, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (4, 'Outros (Sem Cadastro)', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (5, 'Outros (Sem Cadastro)', 5, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (6, 'Outros (Sem Cadastro)', 6, 'GRANDE');
insert into raca (id, nome, especie_id, tipo_porte) values (7, 'Outros (Sem Cadastro)', 7, 'MEDIO');
insert into raca (id, nome, especie_id, tipo_porte) values (8, 'Outros (Sem Cadastro)', 8, 'GRANDE');
insert into raca (id, nome, especie_id, tipo_porte) values (9, 'Outros (Sem Cadastro)', 9, 'MEDIO');
insert into raca (id, nome, especie_id, tipo_porte) values (10, 'Outros (Sem Cadastro)', 10, 'MEDIO');
insert into raca (id, nome, especie_id, tipo_porte) values (11, 'Outros (Sem Cadastro)', 11, 'INDEFINIDO');

--Raças Felinas
insert into raca (id, nome, especie_id, tipo_porte) values (12, 'Siamês', 2, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (13, 'Persa', 2, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (14, 'Exótico', 2, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (15, 'Himalaio', 2, 'PEQUENO');

--Raças Caninas
insert into raca (id, nome, especie_id, tipo_porte) values (16, 'Alano Espanhol', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (17, 'Airedale Terrier', 1, 'INDEFINIDO'); 
insert into raca (id, nome, especie_id, tipo_porte) values (18, 'American Staffordshire Terrier ', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (19, 'American Water Spaniel', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (20, 'Antigo Cão de Pastor Inglês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (21, 'Basset Azul da Gasconha', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (22, 'Basset Fulvo da Bretanha', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (23, 'Basset Hound', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (24, 'Beagle', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (25, 'Bearded Collie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (26, 'Maltês (Bichon Maltês)', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (27, 'Bobtail', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (28, 'Border Collie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (29, 'Boston Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (30, 'Boxer', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (31, 'Bull Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (32, 'Bullmastiff', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (33, 'Bulldog', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (34, 'Cão de Montanha dos Pirinéus', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (35, 'Caniche', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (36, 'Chihuahua', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (37, 'Cirneco do Etna', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (38, 'Chow Chow', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (39, 'Cocker Spaniel (Americano ou Inglês)', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (40, 'Dálmata', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (41, 'Dobermann', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (42, 'Dogue Alemão', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (43, 'Dogue Argentino', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (44, 'Dogue Canário', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (45, 'Fox Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (46, 'Foxhound', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (47, 'Galgo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (48, 'Golden Retriever', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (49, 'Gos d Atura', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (50, 'Husky Siberiano', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (51, 'Laika', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (52, 'Labrador Retriever', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (53, 'Malamute-do-Alasca', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (54, 'Mastin dos Pirenéus', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (55, 'Mastin do Tibete', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (56, 'Mastin Espanhol', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (57, 'Mastín Napolitano', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (58, 'Pastor Alemão', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (59, 'Pastor Belga', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (60, 'Pastor de Brie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (61, 'Pastor dos Pirenéus de Cara Rosa', 1, 'INDEFINIDO'); 
insert into raca (id, nome, especie_id, tipo_porte) values (62, 'Pequinês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (63, 'Perdigueiro', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (64, 'Pitbull', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (65, 'Podengo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (66, 'Pointer', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (67, 'Pug', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (68, 'Rhodesian Ridgeback', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (69, 'Rottweiler', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (70, 'Rough Collie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (71, 'Sabueso (Espanhol ou Italiano)', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (72, 'Saluki', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (73, 'Samoiedo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (74, 'São Bernardo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (75, 'Scottish Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (76, 'Setter Irlandés', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (77, 'Shar-Pei', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (78, 'Shiba Inu', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (79, 'Smooth Collie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (80, 'Staffordshire Bull Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (81, 'Teckel', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (82, 'Terra-nova', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (83, 'Terrier Australiano', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (84, 'Terrier Escocês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (85, 'Terrier Irlandês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (86, 'Terrier Japonês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (87, 'Terrier Negro Russo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (88, 'Terrier Norfolk', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (89, 'Terrier Norwich', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (90, 'Terrier Tibetano', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (91, 'Welhs Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (92, 'West Highland T.', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (93, 'Wolfspitz', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (94, 'Yorkshire Terrier', 1, 'INDEFINIDO');

--Raças Psitacídeos (aves)
insert into raca (id, nome, especie_id, tipo_porte) values (95, 'Papaguaio', 5, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (96, 'Periquitoaustraliano', 5, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (97, 'Cacatua', 5, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (98, 'Calopsita', 5, 'PEQUENO');

--Raças Roedores
insert into raca (id, nome, especie_id, tipo_porte) values (99, 'Hamster', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (100, 'Topolino', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (101, 'Gerbil - Esquilo da Mongólia', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (102, 'Camundongo', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (103, 'Mecol - Twister', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (104, 'Chinchila', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (105, 'Porquinho da Índia', 4, 'PEQUENO');

-- Proprietários
insert into proprietario (id, nome, cpfcnpj, email, telefone, celular)
values (1, 'MARIA DA SILVA','33002233912',null,'(85) 3072-5249','');

insert into proprietario (id, nome, cpfcnpj, email, telefone, celular)
values (2, 'CARLA PINHEIRO','00239244311',null,'(85)  3035-9642','(85) 98151-0157');

-- Animais
insert into animal
(id, castrado, data_nascimento, nome, obito, observacao, pedigree, peso, sexo, especie_id, raca_id, proprietario_id)
values (1,'f','2013-07-02','Yuri','f','','f',2.2,'M',2,12,1);

insert into animal
(id, castrado, data_nascimento, nome,obito, observacao, pedigree, peso, sexo, especie_id, raca_id, proprietario_id)
values (2,'f','2015-06-12','Major','f','','f',0,'F',4,105,1);

insert into animal
(id, castrado, data_nascimento, nome,obito, observacao, pedigree, peso, sexo, especie_id, raca_id, proprietario_id)
values (3,'f','2015-03-10','SINDI','f','','f',0,'F',1,94,2);

insert into animal
(id, castrado, data_nascimento, nome,obito, observacao, pedigree, peso, sexo, especie_id, raca_id, proprietario_id)
values (4,'f','2016-08-09','GABRIELA','f','','f',0.9,'F',5,98,2);

--Atendimentos
insert into atendimento_paciente 
(id, anamnese, data_criacao, data_hora_cancelamento, data_hora_fim, data_hora_inicio,
  diagnostico, exame_complementar2, observacao, peso, prioritario, resultado_exame_complementar2,
  resumo,
  status,
  tipo_atendimento_paciente,
  tratamento,
  animal_id, veterinario_id)
 values  
   (1, 'Queixa Principal: RETORNO PARA MOSTRAR EXAME  , DOACAO IVERMECTINA PARA COMPLETAR 30 DIAS  
Histórico:
Vacinação:
Vermifugação:
Ectoparasitas:
Histórico de Reprodução:
Alimentação:
Doenças Anteriores:
Ambiente:
Procedimentos Realizados',
'2017-03-22',null,'2017-03-22 15:56:21.437','2017-03-22 15:54:26.995','','RASPADO DE PELE E DILUICAO TOTAL LVC ',
'',3,'f','HC ANEMIA DISCRETA, ROULEAUX PTP 9.4','RASPADO DE PELE','ATENDIDO','RETORNO','',1,4)

insert into atendimento_paciente 
(id, anamnese, data_criacao, data_hora_cancelamento, data_hora_fim, data_hora_inicio,
  diagnostico, exame_complementar2, observacao, peso, prioritario, resultado_exame_complementar2,
  resumo,
  status,
  tipo_atendimento_paciente,
  tratamento,
  animal_id,
  veterinario_id)
values
(2, 'Queixa Principal: RETORNOU PARA COLETA DE SANGUE 
Histórico:
Vacinação:
Vermifugação:
Ectoparasitas:
Histórico de Reprodução:
Alimentação:
Doenças Anteriores:
Ambiente:
Procedimentos Realizados:
Contactante:
Medicação utilizada',
'2017-03-22',null,'2017-03-22 15:31:06.185','2017-03-22 15:30:29.248',
null,null,null,null,'f',null,'RETORNO COLETA DE SANGUE','ATENDIDO','RETORNO',null,1,2);

insert into atendimento_paciente 
(id, anamnese, data_criacao, data_hora_cancelamento, data_hora_fim, data_hora_inicio,
  diagnostico, exame_complementar2, observacao, peso, prioritario, resultado_exame_complementar2,
  resumo,
  status,
  tipo_atendimento_paciente,
  tratamento,
  animal_id,
  veterinario_id)
values
(3,'Queixa Principal: desde ontem, no passeio o animal pisou errado e desde então apresenta queixa na pata ( não pisa ). 
Histórico: Tem 5 anos
Vacinação: Não trouxe o cartão
Vermifugação: Atrasada
Ectoparasitas:',
'2017-03-24',null,'2017-03-24 10:29:42.936','2017-03-24 10:15:55.972',
'ruptura de ligamento cruzado?? luxação de patela??','radiografia de pelve e mm posterior esquerdo',
'',8.3,'f','','','ATENDIDO','CLINICO','cronidor',2,2);

insert into atendimento_paciente 
(id, anamnese, data_criacao, data_hora_cancelamento, data_hora_fim, data_hora_inicio,
  diagnostico, exame_complementar2, observacao, peso, prioritario, resultado_exame_complementar2,
  resumo,
  status,
  tipo_atendimento_paciente,
  tratamento,
  animal_id,
  veterinario_id)
values
(4,'Queixa Principal: não melhorou nem comeu. veio para soro e trouxe exame.
Histórico:
Vacinação:
Vermifugação:
Ectoparasitas:
Histórico de Reprodução:
Alimentação:
Doenças Anteriores:
Ambiente:
Procedimentos Realizados:',
'2017-03-24',null,'2017-03-24 10:43:49.808','2017-03-24 10:27:08.112',
'lipidose hepática','solicitado raio x torax e cervical uece',
'',null,'f','hm 10.02; leuc 18.400; plaq. 600.000 ptp 9.4 crat 0.9; alb 1.9; fa 105; tgp 175; us infiltração gordurosa hepatomegalia  infiltração nefrotia gordurosa  discreta quantidade de liq. livre.',
'#','ATENDIDO','RETORNO','soro rl, complexo b, ondansetrona. encaminhada para sonda esofágica',3,4);

