package br.spaixaox.model.enums;

public enum StatusAtendimentoPaciente {

	ESPERA("Em Espera"),
	ATENDIMENTO("Em Atendimento"),
	ATENDIDO("Atendido"),
	CANCELADO("Cancelado");	
	
	private String descricao;
	
	StatusAtendimentoPaciente(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
