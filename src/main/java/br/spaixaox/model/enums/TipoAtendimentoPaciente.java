package br.spaixaox.model.enums;

public enum TipoAtendimentoPaciente {
	CLINICO("Clínico"), 
	CIRURGICO("Cirúrgico"),
	RETORNO("Retorno"),	
	EXAME("Exame");
		
	private String descricao;
	
	TipoAtendimentoPaciente(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
