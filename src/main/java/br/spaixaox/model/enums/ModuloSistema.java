package br.spaixaox.model.enums;

public enum ModuloSistema {
	ATENDIMENTO("atendimento");
		
	private String descricao;
	
	ModuloSistema(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
    
}
