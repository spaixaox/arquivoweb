package br.spaixaox.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.spaixaox.model.Animal;

@Repository
public class AnimalRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager manager;
	
	public Animal porId(Long id) {
		return this.manager.find(Animal.class, id);
	}
	
	public List<Animal> animais(){
		return this.manager.createQuery("from Animal order by nome", Animal.class)
				.getResultList();
	}

	public List<Animal> porNome(String nome) {
			return this.manager.createQuery("from Animal where upper(nome) like :nome order by nome", Animal.class)
					.setParameter("nome", nome.toUpperCase() + "%")
					.getResultList();
	}
}
