package br.spaixaox.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.spaixaox.model.AtendimentoPaciente;
import br.spaixaox.repository.filter.AtendimentoPacienteFilter;

@Repository
public class AtendimentoPacienteRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager manager;
	
	public AtendimentoPaciente guardar(AtendimentoPaciente atendimentoPaciente) {
		return this.manager.merge(atendimentoPaciente);
	}

	public AtendimentoPaciente porId(Long id) {
		return this.manager.find(AtendimentoPaciente.class, id);
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<AtendimentoPaciente> filtrados(AtendimentoPacienteFilter filtro) {


			Session session = this.manager.unwrap(Session.class);

			Criteria criteria = session.createCriteria(AtendimentoPaciente.class)
				// fazemos uma associação (join) com veterinário e nomeamos como "v"
			    .createAlias("veterinario", "v", org.hibernate.sql.JoinType.LEFT_OUTER_JOIN)				
				// fazemos uma associação (join) com animal e nomeamos como "a"
				.createAlias("animal", "a");

		if (filtro.getDataAtendimentoDe() != null) {
			criteria.add(Restrictions.ge("dataCriacao",
					filtro.getDataAtendimentoDe()));
		}
		if (filtro.getDataAtendimentoAte() != null) {
			criteria.add(Restrictions.le("dataCriacao",	filtro.getDataAtendimentoAte()));
		}

		if (filtro.getCodAtendimentoDe() != null) {
			 criteria.add(Restrictions.ge("id",
			 filtro.getCodAtendimentoDe())); 
		}
		
		if (filtro.getCodAtendimentoAte() != null) {
			 criteria.add(Restrictions.le("id",
			 filtro.getCodAtendimentoAte())); 
		}		

		
		if (filtro.getCodigoAnimal() != null) {
			 criteria.add(Restrictions.eq("a.id",
			 filtro.getCodigoAnimal())); 
		}		
		
		if (StringUtils.hasLength( StringUtils.trimAllWhitespace(filtro.getNomeAnimal()))) {
			// acessamos o nome do consultorio associado ao Atendimento de
			// Medicamentos pelo alias "c", criado anteriormente
			criteria.add(Restrictions.ilike("a.nome",
					filtro.getNomeAnimal(), MatchMode.ANYWHERE));
		}		
		
		if (StringUtils.hasLength( StringUtils.trimAllWhitespace(filtro.getNomeVeterinario()))) {
			// acessamos o nome do veterinario associado ao Atendimento de
			// Medicamentos pelo alias "v", criado anteriormente
			criteria.add(Restrictions.ilike("v.nome",
					filtro.getNomeVeterinario(), MatchMode.ANYWHERE));
		}
		
		if (filtro.getStatuses() != null && filtro.getStatuses().length > 0) {
			// adicionamos uma restrição "in", passando um array de constantes
			// da enum StatusPedido
			criteria.add(Restrictions.in("status", filtro.getStatuses()));
		}
		
		if (filtro.getTiposAtendimentos() != null && filtro.getTiposAtendimentos().length > 0) {
			// adicionamos uma restrição "in", passando um array de constantes
			// da enum StatusPedido
			criteria.add(Restrictions.in("tipoAtendimentoPaciente", filtro.getTiposAtendimentos()));
		}		

		//criteria.addOrder(Order.asc("senha"));
		
		return criteria.addOrder(Order.asc("id")).list(); 
	}

	public List<AtendimentoPaciente> porAnimal(Long idAnima) {
		return this.manager.createQuery("from AtendimentoPaciente where animal.id = :idAnimal order by id desc", AtendimentoPaciente.class)
				.setParameter("idAnimal", idAnima)
				.getResultList();
	}
}
