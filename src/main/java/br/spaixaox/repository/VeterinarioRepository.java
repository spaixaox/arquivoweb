package br.spaixaox.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.spaixaox.model.Veterinario;

@Repository
public class VeterinarioRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager manager;
	
	public Veterinario porId(Long id) {
		return this.manager.find(Veterinario.class, id);
	}
	
	public List<Veterinario> veterinarios(){
		return this.manager.createQuery("from Veterinario order by nome", Veterinario.class)
				.getResultList();
	}	

}
