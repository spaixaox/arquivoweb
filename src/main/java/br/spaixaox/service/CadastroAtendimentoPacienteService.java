package br.spaixaox.service;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.spaixaox.model.AtendimentoPaciente;
import br.spaixaox.repository.AtendimentoPacienteRepository;

@Service
public class CadastroAtendimentoPacienteService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	AtendimentoPacienteRepository atendimentoPacienteRepository;

	@Transactional
	public AtendimentoPaciente salvar(AtendimentoPaciente atendimentoPaciente){
	
		if (atendimentoPaciente.isNovo()){
			atendimentoPaciente.setDataCriacao(new Date());
		} else {
			//atendimentoPaciente.setDataHoraAtualizacao(new Date());			
		}
		
		atendimentoPaciente = this.atendimentoPacienteRepository.guardar(atendimentoPaciente);
		return atendimentoPaciente;
		
	}
}
