package br.spaixaox.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.spaixaox.model.UploadArquivo;
import br.spaixaox.repository.UploadArquivoRepository;

@Service
public class CadastroUploadArquivoService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	UploadArquivoRepository uploadArquivoRepository;

	@Transactional
	public UploadArquivo salvar(UploadArquivo uploadArquivo){
	
		uploadArquivo = this.uploadArquivoRepository.guardar(uploadArquivo);
		return uploadArquivo;
		
	}
}
