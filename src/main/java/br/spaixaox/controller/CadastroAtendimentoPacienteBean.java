package br.spaixaox.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.spaixaox.model.Animal;
import br.spaixaox.model.AtendimentoPaciente;
import br.spaixaox.model.UploadArquivo;
import br.spaixaox.model.Veterinario;
import br.spaixaox.model.enums.StatusAtendimentoPaciente;
import br.spaixaox.model.enums.TipoAtendimentoPaciente;
import br.spaixaox.repository.AnimalRepository;
import br.spaixaox.repository.AtendimentoPacienteRepository;
import br.spaixaox.repository.UploadArquivoRepository;
import br.spaixaox.repository.VeterinarioRepository;
import br.spaixaox.service.CadastroAtendimentoPacienteService;
import br.spaixaox.util.jsf.FacesUtil;

@Controller
@Scope("view")
public class CadastroAtendimentoPacienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private AtendimentoPaciente atendimentoPaciente;

	
	private List<UploadArquivo> uploadArquivos;
	private List<Veterinario> veterinarios;
	private List<AtendimentoPaciente> historicoAtendimentosPaciente;
	private String anamnese;
	private String receituario;
	private UploadArquivo uploadArquivo;

	@Autowired
	private AnimalRepository animalRepository;

	@Autowired
	private AtendimentoPacienteRepository atendimentoPacienteRepository;	
	
	@Autowired
	private VeterinarioRepository veterinarioRepository;

	@Autowired
	private CadastroAtendimentoPacienteService cadastroAtendimentoPacienteService;

	@Autowired
	private UploadArquivoRepository uploadArquivoRepository;
	
	@Autowired
	private UploadAvancadoBean uploadAvancadoBean;
	
	public UploadArquivo getUploadArquivo() {
		return uploadArquivo;
	}

	public void setUploadArquivo(UploadArquivo uploadArquivo) {
		this.uploadArquivo = uploadArquivo;
	}

	public AtendimentoPaciente getAtendimentoPaciente() {
		return atendimentoPaciente;
	}

	public void setAtendimentoPaciente(
			AtendimentoPaciente atendimentoPaciente) {
		this.atendimentoPaciente = atendimentoPaciente;
	}
	
	public List<UploadArquivo> getUploadArquivos() {
		return uploadArquivos;
	}

	public List<Veterinario> getVeterinarios() {
		return veterinarios;
	}

	public void setVeterinarios(List<Veterinario> veterinarios) {
		this.veterinarios = veterinarios;
	}

	public List<AtendimentoPaciente> getHistoricoAtendimentosPaciente() {
		return historicoAtendimentosPaciente;
	}

	public void setHistoricoAtendimentosPaciente(
			List<AtendimentoPaciente> historicoAtendimentosPaciente) {
		this.historicoAtendimentosPaciente = historicoAtendimentosPaciente;
	}
	
	public String getAnamnese() {
		return anamnese;
	}

	public void setAnamnese(String anamnese) {
		this.anamnese = anamnese;
	}

	public String getReceituario() {
		return receituario;
	}

	public void setReceituario(String receituario) {
		this.receituario = receituario;
	}

	public CadastroAtendimentoPacienteBean() {
		limpar();
	}

	public StatusAtendimentoPaciente[] getStatusAtendimentoPaciente(){
	     return StatusAtendimentoPaciente.values();
	}

	public TipoAtendimentoPaciente[] gettipoAtendimentoPaciente(){
	     return TipoAtendimentoPaciente.values();
	}	

	/*
	private void carregarVeterinarioLogado(){
		if (this.isUsuarioVeterinario() && this.atendimentoPaciente.getVeterinario() == null){
			if (seguranca.getVeterinarioUsuario() != null){
				this.atendimentoPaciente.setVeterinario(seguranca.getVeterinarioUsuario());					
			}			
		} 
	}
	*/
	
    public void upload(FileUploadEvent event) {
    	//uploadArquivo.setDescricao(descricaoArquivo);
    	System.out.println("uploadArquivo.getDescricao(): "+uploadArquivo.getDescricao());
    	uploadAvancadoBean.upload(event, this.atendimentoPaciente, uploadArquivo);
    	listarArquivos();
    	uploadArquivo = new UploadArquivo();
    }
    
    public void listarArquivos() {
    	//public List<File> listar(ModuloSistema modulo, Long idAtendimento) {
    	//arquivos = uploadAvancadoBean.listarArquivos(ModuloSistema.ATENDIMENTO, this.atendimentoPaciente.getId()); ;
    	this.uploadArquivos = this.uploadArquivoRepository.porAtendimento(this.atendimentoPaciente.getId());
    }
    
    public boolean existeArquivo(String caminhoDiretorio, String nomeArquivo){
    	return uploadAvancadoBean.existeArquivo(caminhoDiretorio, nomeArquivo);
    }
    
	public boolean naoExisteArquivo(String caminhoDiretorio, String nomeArquivo){
		return !existeArquivo(caminhoDiretorio, nomeArquivo);
    }    
    
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			listarArquivos();
			this.veterinarios = this.veterinarioRepository.veterinarios();
			Long idAnima = this.atendimentoPaciente.getAnimal().getId();
			this.historicoAtendimentosPaciente = this.atendimentoPacienteRepository.porAnimal(idAnima);
           //carregarVeterinarioLogado();			
		}
	}

	public void limpar() {
		atendimentoPaciente = new AtendimentoPaciente();
		uploadArquivo = new UploadArquivo();
	}
/*
	public void carregarPatologias(){
		if (this.atendimentoPaciente.getAnimal() != null){
			this.patologias = this.patologiaRepository.porEspecie(this.atendimentoPaciente.getAnimal().getEspecie().getId());
		}
	}
*/	
	public void salvar() {
		
		try {
			this.atendimentoPaciente = this.cadastroAtendimentoPacienteService.salvar(this.atendimentoPaciente);

			FacesUtil.addInfoMessage("Atendimento salvo com sucesso!");
		} catch (Exception e) {
			FacesUtil.addErrorMessage("Atendimento NÃO foi salvo. Mensagem: "+e.getMessage()+e.getStackTrace().toString()+e.getCause());
		}
	}
	
	public void cancelarAtendimento() {
		try {
			this.atendimentoPaciente.setDataHoraCancelamento(new Date());
			this.atendimentoPaciente.setStatus(StatusAtendimentoPaciente.CANCELADO);
			salvar();
			FacesUtil.addInfoMessage("Atendimento cancelado com sucesso!");			
		} catch (Exception e) {
			FacesUtil.addErrorMessage("Atendimento não foi cancelado. Mensagem: "+e.getMessage());
		}
	}
	
	private void iniciarCamposAtendimento(){
		/*
		if (this.atendimentoPaciente.isEmEspera()){
			for (Configuracao configuracao : configuracoes) {
				if (configuracao.getChave().equals("ATENDIMENTO_ANAMNESE") && this.atendimentoPaciente.getAnamnese().isEmpty()){
					this.atendimentoPaciente.setAnamnese(configuracao.getValor());
				} else if (configuracao.getChave().equals("ATENDIMENTO_RECEITUARIO")){
					this.atendimentoPaciente.setReceituario(configuracao.getValor());
				} else if (configuracao.getChave().equals("ATENDIMENTO_AVALIACAO_FISICA") && this.atendimentoPaciente.getExameFisico().isEmpty()) {
					this.atendimentoPaciente.setExameFisico(configuracao.getValor());					
				}
			}
		}*/
	}
	
	public void iniciarAtendimento() {

		try {
			  if(this.atendimentoPaciente.isPodeIniciar()){
					this.atendimentoPaciente.setDataHoraInicio(new Date());
					iniciarCamposAtendimento();
					this.atendimentoPaciente.setStatus(StatusAtendimentoPaciente.ATENDIMENTO);
					
					salvar();
					FacesUtil.addInfoMessage("Atendimento iniciado com sucesso.");
			  }
		} catch (Exception e) {
			FacesUtil.addErrorMessage("Atendimento não foi iniciado. Mensagem: "+e.getMessage());			
		}

	}
	
	public void finalizarAtendimento() {
		
		try {
			if (this.atendimentoPaciente.isPodeFinalizar()){
				this.atendimentoPaciente.setDataHoraFim(new Date());
				this.atendimentoPaciente.setStatus(StatusAtendimentoPaciente.ATENDIDO);
				
				salvar();
				FacesUtil.addInfoMessage("Atendimento finalizado com sucesso.");
			}
			
		} catch (Exception e) {
			FacesUtil.addErrorMessage("Atendimento não foi finalizado. Mensagem: "+e.getMessage());
			System.out.println("Atendimento não foi Finalizado"+e.getMessage());
		}
		
	}
	
	public List<Animal> completarAnimal(String nome) {
		return this.animalRepository.porNome(nome);
	}
	
	public boolean isEditando() {
		return this.atendimentoPaciente.getId() != null;
	}
	
}
