package br.spaixaox.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.spaixaox.model.AtendimentoPaciente;
import br.spaixaox.model.UploadArquivo;
import br.spaixaox.model.enums.ModuloSistema;
import br.spaixaox.service.CadastroUploadArquivoService;
import br.spaixaox.util.ArquivoUtil;

@Controller
@Scope("view")
public class UploadAvancadoBean implements Serializable {

	private static final long serialVersionUID = 1L;

    private StreamedContent streamedContent;
	private UploadedFile uploadedFile;
    private List<File> arquivos = new ArrayList<>();
   // private ModuloSistema moduloSistema = ModuloSistema.ATENDIMENTO;
	    
    private Long idAtendimento = new Long(1052);
	 
	private String caminhoDirRaizTomcat = System.getProperty("catalina.base");
	ArquivoUtil arquivoUtil;
	
	@Autowired
	private CadastroUploadArquivoService cadastroUploadArquivoService;

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}

	public String getCaminhoDirRaizTomcat() {
		return caminhoDirRaizTomcat;
	}

	@PostConstruct
	    public void postConstruct() {
    	    arquivoUtil = new ArquivoUtil();		
	        arquivos = new ArrayList<>(arquivoUtil.listar(ModuloSistema.ATENDIMENTO, idAtendimento));
	}	 
	
	public void listarArquivos(){
		arquivos = new ArrayList<>(arquivoUtil.listarTeste(idAtendimento));
	}

	public List<File> listarArquivos(ModuloSistema moduloSistema, Long idAtendimento ){
		arquivos = new ArrayList<>(arquivoUtil.listar(moduloSistema, idAtendimento));
		return arquivos;
	}	

	public void listarArquivosPorAtendimento(Long idAtendimento){
		arquivos = new ArrayList<>(arquivoUtil.listarTeste(idAtendimento));
	}

	public boolean existeArquivo(String caminhoDiretorio, String nomeArquivo){
		String caminhoArquivo = caminhoDiretorio+"/"+nomeArquivo;
    	return arquivoUtil.existeArquivo(caminhoArquivo);
    }
	
	public boolean naoExisteArquivo(String caminhoDiretorio, String nomeArquivo){
		return !existeArquivo(caminhoDiretorio, nomeArquivo);
    }
	
    public void download(String caminhoDiretorios, String nomeArquivo) throws IOException{
    	File file = new File(caminhoDiretorios, nomeArquivo);

        InputStream inputStream = new FileInputStream(file);
        
        streamedContent = new DefaultStreamedContent(inputStream, 
                Files.probeContentType(file.toPath()), file.getName());

    }
	
	
    public void upload(FileUploadEvent event) {
    	
        UploadedFile uploadedFile = event.getFile();
        
		Long idAnimal = new Long(241);
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Fortaleza"));
        Date dataHora =  calendar.getTime();

		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
		String dataHoraFormatada = format.format(dataHora);

		/*Formação do nome do Arquivo Destino 
		 * -> idAnimal_idAtendimento_yyyymmdd_HHmmssSSS.<extensão original>
		 *  PS. SSS são milisegundos
		 * Tabela upload_arquivo (id, data_hora_criacao, content_type, substituido (=false), usuario_id, animal_id, atendimento_id,
		 *                modulo, nome_origem, nome_destino, caminho_diretorio, data_hora_exclusao)
		 *   Validação: verificar se nomeOrigem já existe para o mesmo idAnimal, idAtendimento -> substituido = true
		 */
		
		try {
			
			String nomeArquivoOrigem = uploadedFile.getFileName();
			
			String pathDiretorios = arquivoUtil.montarPath(ModuloSistema.ATENDIMENTO, idAtendimento);
			File fileOrigem = new File(pathDiretorios, nomeArquivoOrigem);
			//File fileOrigem = new File(arquivoUtil.diretorioRaizParaArquivos(moduloSistema, idAtendimento), nomeArquivoOrigem);
			String extensao = nomeArquivoOrigem.substring(nomeArquivoOrigem.lastIndexOf("."), nomeArquivoOrigem.length());
			
			String nomeArquivoDestino = String.valueOf(idAnimal) +"_"+
                    String.valueOf(idAtendimento) +"_"+
                    dataHoraFormatada + extensao;			

			File fileDestino = new File(pathDiretorios, nomeArquivoDestino);
			//File fileDestino = new File(arquivoUtil.diretorioRaizParaArquivos(moduloSistema, idAtendimento), nomeArquivoDestino);

			fileDestino.renameTo(fileOrigem);
			
			OutputStream out = new FileOutputStream(fileDestino);
			out.write(uploadedFile.getContents());
			out.close();
			
			String contentType = uploadedFile.getContentType();
			caminhoDirRaizTomcat = "contentType: "+contentType+" pathDiretorios: "+pathDiretorios;
			
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Upload do arquivo "+nomeArquivoOrigem+" Completo", "O arquivo "+ fileOrigem + " foi salvo!"));
			
	        arquivos = new ArrayList<>(arquivoUtil.listar(ModuloSistema.ATENDIMENTO, idAtendimento));			
			
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null, 
					  new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro", e.getMessage()));
		}
		
	}
    
    public void upload(FileUploadEvent event, AtendimentoPaciente atendimentoPaciente, UploadArquivo uploadArquivo) {
    	
        UploadedFile uploadedFile = event.getFile();
        
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Fortaleza"));
        Date dataHora =  calendar.getTime();

		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
		String dataHoraFormatada = format.format(dataHora);

		/*Formação do nome do Arquivo Destino 
		 * -> idAnimal_idAtendimento_yyyymmdd_HHmmssSSS.<extensão original>
		 *  PS. SSS são milisegundos
		 * Tabela upload_arquivo (id, data_hora_criacao, content_type, substituido (=false), usuario_id, animal_id, atendimento_id,
		 *                modulo, nome_origem, nome_destino, caminho_diretorio, data_hora_exclusao)
		 *   Validação: verificar se nomeOrigem já existe para o mesmo idAnimal, idAtendimento -> substituido = true
		 */
		
		try {
			
			String nomeArquivoOrigem = uploadedFile.getFileName();
			
			String pathDiretorios = arquivoUtil.montarPath(ModuloSistema.ATENDIMENTO, atendimentoPaciente.getId());
			File fileOrigem = new File(pathDiretorios, nomeArquivoOrigem);
			//File fileOrigem = new File(arquivoUtil.diretorioRaizParaArquivos(moduloSistema, idAtendimento), nomeArquivoOrigem);
			String extensao = nomeArquivoOrigem.substring(nomeArquivoOrigem.lastIndexOf("."), nomeArquivoOrigem.length());
			
			String nomeArquivoDestino = String.valueOf(atendimentoPaciente.getAnimal().getId()) +"_"+
                    String.valueOf(atendimentoPaciente.getId()) +"_"+
                    dataHoraFormatada + extensao;			

			File fileDestino = new File(pathDiretorios, nomeArquivoDestino);

			fileDestino.renameTo(fileOrigem);
			
			OutputStream out = new FileOutputStream(fileDestino);
			out.write(uploadedFile.getContents());
			out.close();
			
			String contentType = uploadedFile.getContentType();
			caminhoDirRaizTomcat = "contentType: "+contentType+" pathDiretorios: "+pathDiretorios;
			
	    	if (uploadArquivo == null){
	    		System.out.println("Forçando a criação do objeto uploadArquivo");
	    		uploadArquivo = new UploadArquivo();
	    		uploadArquivo.setDescricao("descrição automática");
	    	}

	    	System.out.println("uploadArquivo.getDescricao: "+uploadArquivo.getDescricao());
	        uploadArquivo.setAtendimentoPaciente(atendimentoPaciente);
	        uploadArquivo.setDataHoraUpload(new Date());
	        uploadArquivo.setContentType(contentType);
	        uploadArquivo.setNomeOrigem(nomeArquivoOrigem);
	        uploadArquivo.setNomeDestino(nomeArquivoDestino);
	        uploadArquivo.setCaminhoDiretorio(pathDiretorios);
	        uploadArquivo.setModuloSistema(ModuloSistema.ATENDIMENTO);
	        uploadArquivo = cadastroUploadArquivoService.salvar(uploadArquivo);
			
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Upload do arquivo "+nomeArquivoOrigem+" Completo", "O arquivo "+ fileOrigem + " foi salvo!"));
			
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null, 
					  new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro", e.getMessage()));
		}
		
	}
    

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public List<File> getArquivos() {
		return arquivos;
	}

	public Long getIdAtendimento() {
		return idAtendimento;
	}

	public void setIdAtendimento(Long idAtendimento) {
		this.idAtendimento = idAtendimento;
	}

	/*
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Fortaleza"));
		int ano = calendar.get(Calendar.YEAR);
		int mes = calendar.get(Calendar.MONTH) + 1; // O mês vai de 0 a 11.
		int semana = calendar.get(Calendar.WEEK_OF_MONTH);
		int dia = calendar.get(Calendar.DAY_OF_MONTH);
		int hora = calendar.get(Calendar.HOUR_OF_DAY);
		int minuto = calendar.get(Calendar.MINUTE);
		int segundo = calendar.get(Calendar.SECOND);	 
	 */
	
	
	
}
