package br.spaixaox.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.spaixaox.model.enums.ModuloSistema;
import br.spaixaox.util.ArquivoUtil;

@Controller
@Scope("view")
public class UploadBasicoBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	ArquivoUtil arquivoUtil;

	private UploadedFile uploadedFile;
    private ModuloSistema moduloSistema = ModuloSistema.ATENDIMENTO;
    
    private Long idAtendimento = new Long(1052);
	
	private String caminhoDirRaizTomcat = System.getProperty("catalina.base");
	
	public UploadBasicoBean() {
		arquivoUtil = new ArquivoUtil();
	}

	public String getCaminhoDirRaizTomcat() {
		return caminhoDirRaizTomcat;
	}

	public void upload(){
		
		Long idAnimal = new Long(241);
		Long idAtendimento = new Long(1052);
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Fortaleza"));
        Date dataHora =  calendar.getTime();

		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String dataHoraFormatada = format.format(dataHora);

		/*Formação do nome do Arquivo Destino 
		 * -> idAnimal_idAtendimento_yyyymmdd_HHmmss.<extensão original>
		 * Tabela upload_arquivo (id, data_hora_criacao, content_type, substituido (=false), usuario_id, animal_id, atendimento_id,
		 *                modulo, nome_origem, nome_destino, caminho_diretorio, data_hora_exclusao)
		 *   Validação: verificar se nomeOrigem já existe para o mesmo idAnimal, idAtendimento -> substituido = true
		 */
		
		try {
			
			String nomeArquivoOrigem = uploadedFile.getFileName();
	
			String pathDiretorios = arquivoUtil.montarPath(moduloSistema, idAtendimento);
			File fileOrigem = new File(pathDiretorios, nomeArquivoOrigem);			
			//File fileOrigem = new File(ArquivoUtil.diretorioRaizParaArquivos(moduloSistema, idAtendimento), nomeArquivoOrigem);
			String extensao = nomeArquivoOrigem.substring(nomeArquivoOrigem.lastIndexOf("."), nomeArquivoOrigem.length());
			
			String nomeArquivoDestino = String.valueOf(idAnimal) +"_"+
                    String.valueOf(idAtendimento) +"_"+
                    dataHoraFormatada + extensao;			

			File fileDestino = new File(pathDiretorios, nomeArquivoDestino);
			//File fileDestino = new File(arquivoUtil.diretorioRaizParaArquivos(moduloSistema, idAtendimento), nomeArquivoDestino);

			fileDestino.renameTo(fileOrigem);
			
			OutputStream out = new FileOutputStream(fileDestino);
			out.write(uploadedFile.getContents());
			out.close();
			
			String contentType = uploadedFile.getContentType();
			caminhoDirRaizTomcat = "contentType: "+contentType+" pathDiretorios: "+pathDiretorios;			
			
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Upload do arquivo "+nomeArquivoOrigem+" Completo", "O arquivo "+ fileOrigem + " foi salvo!"));
			
			System.out.println("O arquivo "+ nomeArquivoDestino + " foi salvo!");
			
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null, 
					  new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro", e.getMessage()));
		}
		
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public Long getIdAtendimento() {
		return idAtendimento;
	}

	public void setIdAtendimento(Long idAtendimento) {
		this.idAtendimento = idAtendimento;
	}

	/*
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Fortaleza"));
		int ano = calendar.get(Calendar.YEAR);
		int mes = calendar.get(Calendar.MONTH) + 1; // O mês vai de 0 a 11.
		int semana = calendar.get(Calendar.WEEK_OF_MONTH);
		int dia = calendar.get(Calendar.DAY_OF_MONTH);
		int hora = calendar.get(Calendar.HOUR_OF_DAY);
		int minuto = calendar.get(Calendar.MINUTE);
		int segundo = calendar.get(Calendar.SECOND);	 
	 */
	
}
