package br.spaixaox.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.spaixaox.model.AtendimentoPaciente;
import br.spaixaox.model.enums.StatusAtendimentoPaciente;
import br.spaixaox.model.enums.TipoAtendimentoPaciente;
import br.spaixaox.repository.AtendimentoPacienteRepository;
import br.spaixaox.repository.filter.AtendimentoPacienteFilter;

@Controller
@Scope("view")
public class PesquisaAtendimentoPacienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private AtendimentoPacienteRepository atendimentoPacienteRepository;
	
	private AtendimentoPacienteFilter filtro;

	private List<AtendimentoPaciente> atendimentosFiltrados;
	
	private String totaisAtendimentos;
	private int totalEmEspera = 0;
	private int totalEmAtendimento = 0;
	private int totalAtendido = 0;
	private int totalCancelado = 0;

	public PesquisaAtendimentoPacienteBean() {
		filtro = new AtendimentoPacienteFilter();
		atendimentosFiltrados = new ArrayList<>();
		//createHorizontalBarModel();
	}
	
	public String getTotaisAtendimentos() {
		return totaisAtendimentos;
	}

	public void pesquisar() {
		atendimentosFiltrados = atendimentoPacienteRepository.filtrados(filtro);
		carregarTotaisAtendimentos();
	}

	/*
	public void pesquisarEmEspera() {
		filtro.setDataAtendimentoDe(new Date());
		filtro.setDataAtendimentoAte(new Date());
		atendimentosFiltrados = atendimentoPacienteRepository.filtradosEmEspera(filtro);
	}*/
	
	public StatusAtendimentoPaciente[] getStatuses() {
		return StatusAtendimentoPaciente.values();
	}

	public TipoAtendimentoPaciente[] getTiposAtendimentos() {
		return TipoAtendimentoPaciente.values();
	}	
	
	public List<AtendimentoPaciente> getAtendimentosFiltrados() {
		return atendimentosFiltrados;
	}

	public AtendimentoPacienteFilter getFiltro() {
		return filtro;
	}
	
	private void carregarTotaisAtendimentos(){

		totalEmEspera = 0;
		totalEmAtendimento = 0;
		totalAtendido = 0;
		totalCancelado = 0;
		
		for (AtendimentoPaciente atendimento : this.atendimentosFiltrados) {

			if (atendimento.getStatus().toString().equals(StatusAtendimentoPaciente.ESPERA.toString())){
				totalEmEspera = totalEmEspera + 1;
			} else if (atendimento.getStatus().toString().equals(StatusAtendimentoPaciente.ATENDIMENTO.toString())){
				totalEmAtendimento++;
			} else if (atendimento.getStatus().toString().equals(StatusAtendimentoPaciente.ATENDIDO.toString())){
				totalAtendido++;				
			} else if (atendimento.getStatus().toString().equals(StatusAtendimentoPaciente.CANCELADO.toString())){
				totalCancelado++;
			}
		}
		
		this.totaisAtendimentos = "Em Espera: "+String.valueOf(totalEmEspera)+"     |"+
				                  "Em Atendimento: "+String.valueOf(totalEmAtendimento)+"      |"+
				                  "Atendido(s): "+String.valueOf(totalAtendido)+"     |"+
				                  "Cancelado(s): "+String.valueOf(totalCancelado);

	}

	public int getTotalEmEspera() {
		return totalEmEspera;
	}

	public int getTotalEmAtendimento() {
		return totalEmAtendimento;
	}

	public int getTotalAtendido() {
		return totalAtendido;
	}

	public int getTotalCancelado() {
		return totalCancelado;
	}
	
	/*
	public boolean isUsuarioVeterinario() {
		return seguranca.getTipoGrupoUsuario().toUpperCase().equals("VETERINARIO");
	}*/
	
	/*
	public boolean isUsuarioVeterinarioCirurgiao(){
		if (isUsuarioVeterinario()){
			return seguranca.getVeterinarioUsuario().isCirurgiao();
		} 
		return false;
	}*/	
	/*
	public boolean usuarioPodeAtenderTipoCirurgico(boolean isTipoAtendimentoCirurgico){
		
		if (!isTipoAtendimentoCirurgico){
			return true;
		}else { //é Tipo Atendimento Cirúrgico
			if (!isUsuarioVeterinario()){
				return true;
			} else { //usuário é Veterinário
			    if (isUsuarioVeterinarioCirurgiao()){
					return true;
				} else {
				    return false;			
				}				
			}

		}
	}*/

}
