package br.spaixaox.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.spaixaox.util.ArquivoUtil;

@Controller
@Scope("view")
public class DownloadBean implements Serializable {

	private static final long serialVersionUID = 1L;
	StreamedContent streamedContent;
	
	ArquivoUtil arquivoUtil;
	
    public DownloadBean() {
    	arquivoUtil = new ArquivoUtil();	
	}

	public boolean existeArquivo(String caminhoDiretorio, String nomeArquivo){
		String caminhoArquivo = caminhoDiretorio+"/"+nomeArquivo;
    	return arquivoUtil.existeArquivo(caminhoArquivo);
    }
	
	public boolean naoExisteArquivo(String caminhoDiretorio, String nomeArquivo){
		return !existeArquivo(caminhoDiretorio, nomeArquivo);
    }
	
    
	public boolean existeArquivo(File file){
    	return arquivoUtil.existeArquivo(file.getAbsolutePath());
    }

    public boolean naoExisteArquivo(File file){
    	//System.out.println("file.getName(): "+file.getName()); apenas o nome
    	//System.out.println("file.getAbsolutePath(): "+file.getAbsolutePath()); o caminho e o nome    	
    	return !(arquivoUtil.existeArquivo(file.getAbsolutePath()));
    }
    
    
    public void download(String caminhoDiretorios, String nomeArquivo) throws IOException{
    	File file = new File(caminhoDiretorios, nomeArquivo);

        InputStream inputStream = new FileInputStream(file);
        
        streamedContent = new DefaultStreamedContent(inputStream, 
                Files.probeContentType(file.toPath()), file.getName());

    }
    
    public void download(File file) throws IOException {
    	existeArquivo(file);
    	
        InputStream inputStream = new FileInputStream(file);
        
        streamedContent = new DefaultStreamedContent(inputStream, 
                Files.probeContentType(file.toPath()), file.getName());

    }
    
    public StreamedContent getStreamedContent() {
        return streamedContent;
    }

}
