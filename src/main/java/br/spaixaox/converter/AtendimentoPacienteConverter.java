package br.spaixaox.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.stereotype.Component;

import br.spaixaox.model.AtendimentoPaciente;
import br.spaixaox.repository.AtendimentoPacienteRepository;

@Component
@FacesConverter(forClass = AtendimentoPaciente.class)
public class AtendimentoPacienteConverter implements Converter {

	private AtendimentoPacienteRepository atendimentoPacienteRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		AtendimentoPaciente retorno = null;
		
		if (value == null || value.isEmpty()) {
			return retorno;
		} 

		Long id = new Long(value);
		
			
		atendimentoPacienteRepository = (AtendimentoPacienteRepository)context
				                         .getELContext().getELResolver()
				                         .getValue(context.getELContext(), null, "atendimentoPacienteRepository");

		/*
		atendimentoPacienteRepository = context.getApplication()
				                        .evaluateExpressionGet(context,
				                        "#{atendimentoPacienteRepository}", 
				                        AtendimentoPacienteRepository.class);
		*/
		retorno = atendimentoPacienteRepository.porId(id);
	
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		
		AtendimentoPaciente atendimentoPaciente = (AtendimentoPaciente) value;
		return atendimentoPaciente.getId().toString();
		
	}

}
