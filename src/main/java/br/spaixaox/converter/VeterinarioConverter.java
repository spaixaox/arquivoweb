package br.spaixaox.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.spaixaox.model.Veterinario;
import br.spaixaox.repository.AtendimentoPacienteRepository;
import br.spaixaox.repository.VeterinarioRepository;

@FacesConverter(forClass = Veterinario.class)
public class VeterinarioConverter implements Converter {

	private VeterinarioRepository veterinarioRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Veterinario retorno = null;
		
		if (value == null || value.isEmpty()) {
			return retorno;
		} 

		Long id = new Long(value);
		
		
		veterinarioRepository = (VeterinarioRepository)context
                .getELContext().getELResolver()
                .getValue(context.getELContext(), null, "veterinarioRepository");		
		
		/*
		veterinarioRepository = context.getApplication()
				                        .evaluateExpressionGet(context,
				                        "#{veterinarioRepository}", 
				                        VeterinarioRepository.class);
		*/
		retorno = veterinarioRepository.porId(id);
	
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		
		Veterinario veterinario = (Veterinario) value;
		return veterinario.getId().toString();
		
	}

}
