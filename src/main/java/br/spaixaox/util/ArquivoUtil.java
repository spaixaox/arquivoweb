package br.spaixaox.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.spaixaox.conf.ConfiguracaoWeb;
import br.spaixaox.model.enums.ModuloSistema;

public class ArquivoUtil {

	
	private String getNomeSistema(){
		ConfiguracaoWeb conf = new ConfiguracaoWeb();
		String nomeSistema = conf.getSistemaNome();
		return nomeSistema;
	}

	//Talvez seja interessante ao invés de deletar direto, mover para uma pasta temporária.
	//os arquivos poderão ser excluidos (rotina interna automática) após 1 mês|semana
	public boolean excluirArquivo(String caminhoArquivo) throws IOException{
		Path arquivo = Paths.get(caminhoArquivo);
		return Files.deleteIfExists(arquivo);
	}

	public boolean existeArquivo(String caminhoArquivo){
		Path arquivo = Paths.get(caminhoArquivo);
		return Files.exists(arquivo);
	}	
	
	/*
    public static File escrever(String name, ModuloSistema modulo, byte[] contents) throws IOException {
        File file = new File(diretorioRaizParaArquivos(modulo), name);

        OutputStream out = new FileOutputStream(file);
        out.write(contents);
        out.close();

        return file;
    }
    */
	
	
    public List<File> listarTeste(Long idAtendimento) {
        //Simular caso o path completo venha de um banco de dados (inicialmente paths státicos)    	
    	List<File> arquivos = new ArrayList<>();
        String filePath1 = "/opt/apache-tomcat-8.0.39/UploadsArquivos/arquivoweb/atendimento/1051/241_1051_20170419_022423760.jpg";
        String filePath2 = "/opt/apache-tomcat-8.0.39/UploadsArquivos/arquivoweb/atendimento/1052/241_1052_20170419_022452953.jpg";
        String filePath3 = "/opt/apache-tomcat-8.0.39/UploadsArquivos/arquivoweb/atendimento/1053/241_1053_20170419_022521571.jpeg";
        String filePath4 = "/opt/apache-tomcat-8.0.39/UploadsArquivos/arquivoweb/atendimento/1051/241_1051_20170419_02242376X.jpg";        

        if (existeArquivo(filePath1)){
           	File file1 = new File(filePath1);
           	arquivos.add(file1);        	
        }
        
        if (existeArquivo(filePath2)) {
           	File file2 = new File(filePath2);
           	arquivos.add(file2);
        }

        if (existeArquivo(filePath3)) {
           	File file3 = new File(filePath3);
           	arquivos.add(file3);
        }        
        
        //este arquivo não existe -> desabilitar opção de Download na tela e informar que é inexistente
       	File file4 = new File(filePath4);
       	arquivos.add(file4);        
        
        return arquivos;
    }

    /*
    public List<UploadArquivo> listarPorAtendimento(List<UploadArquivo> uploadArquivos) {
        //Simular caso o path completo venha de um banco de dados (inicialmente paths státicos)    	
    	List<UploadArquivo> arquivos = new ArrayList<>();
        String arquivo = null;
    	File fileAux = null;
    	
    	for (UploadArquivo uploadArquivo : uploadArquivos) {
			arquivo = uploadArquivo.getCaminhoDiretorio()+"/"+uploadArquivo.getNomeDestino(); 
			if (existeArquivo(arquivo)){
				fileAux = new File(arquivo); 
				//arquivos.add(fileAux);
			}
		}
		
        return arquivos;
    }*/
    
	
	
    public List<File> listar(ModuloSistema modulo, Long idAtendimento) {

    	String pathDiretorios = montarPath(modulo, idAtendimento);
    	System.out.println("listar->pathDiretorios: "+pathDiretorios);
    	File dir = diretorioRaizParaArquivos(pathDiretorios);
        //File dir = diretorioRaizParaArquivos(modulo, idAtendimento);

        return Arrays.asList(dir.listFiles());
    }

	public String montarPath(ModuloSistema moduloSistema, Long idAtendimento){
		String path = null;
		String dirNomeApp = null;
		String dirNomeModulo = null;
		String dirIdAtendimento = null;

		if(moduloSistema == ModuloSistema.ATENDIMENTO){
			System.out.println("montarPath: entrou no modulo Atendimento");
	    	dirNomeApp       = getNomeSistema().toLowerCase();
	    	dirNomeModulo    = moduloSistema.name().toLowerCase();
	    	dirIdAtendimento = String.valueOf(idAtendimento);
	    	path = diretorioRaiz()+"/"+dirNomeApp+"/"+dirNomeModulo+"/"+dirIdAtendimento;
	    	//ex: /<raizDoTomCat / UploadsArquivos>/<nome da aplicação>/<nome do módulo>/<id Atendimento> 
	    	//ex: /opt/apache-tomcat/UploadsArquivos/arquivoweb/atendimento/1051/"
		} else {
			System.out.println("montarPath: entrou no modulo não informado, diferente de Atendimento");			
	    	dirNomeApp       = getNomeSistema().toLowerCase();
	    	dirNomeModulo    = moduloSistema.name().toLowerCase();
	    	path = diretorioRaiz()+"/"+dirNomeApp+"/"+dirNomeModulo;
		}
		
		criarEstruturaParaUpload(path);//Cria diretórios		
		
		return path;
	}    

    public File criarEstruturaParaUpload(String path) {
        File dir = new File(path);
        
        System.out.println("criarEstruturaParaUpload->path :"+path);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        return dir;
    }
	
	
    public File diretorioRaizParaArquivos(String path) {
        File dir = new File(path);
        
        System.out.println("diretorioRaizParaArquivos->path :"+path);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        return dir;
    }

    public File diretorioRaiz() {

         File dir = new File(System.getProperty("catalina.base"), "UploadsArquivos");
         //irá criar(se necessário) e retornar:  /opt/apache-tomcat/UploadsArquivos
         
         //System.getProperty("user.home")
         //System.getProperty("user.dir");         
         //System.getProperty("catalina.base");         
         //System.getProperty("java.io.tmpdir");

        if (!dir.exists()) {
            dir.mkdirs();
        }

        return dir;
    }
}
