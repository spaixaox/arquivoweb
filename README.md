ArquivoWeb é um projeto desenvolvido em JSP 2.2, Spring Framework, JPA com Hibernate 2.0 e PostgreSql 9.3.16. Ambiente com Java 1.7, Tomcat 8.0.39.

Outras informações: banco de dados chamado "arquivos-web" e dados básicos contidos no arquivo "script_criacao.sql" (insert de registros de Proprietários, Animais, Veterinários, Espécies, Raças, Atendimentos e outros). 

Projeto foi desenvolvido para testar Upload e Download de arquivos, funcionalidades estas que serão reaproveitadas em outro projeto, um Sistema para Hospital Veterinário (está em produção num cliente). Para ficar mais próximo da integração foi incluido parte do módulo de Atendimento do sistema, uma versão resumida.

A ideia é que para cada atendimento (para cada id de Atendimento) o Veterinário possa realizar Upload e Download de fotos dos animais (pacientes). Por este motivo que as estruturas de diretórios seguem a seguinte lógica: <raiz do tomcat>/UploadsArquivos/<nome do projeto>/<nome do módulo>/<id do atendimento>
Ex: apache-tomcat-8.0.39/UploadsArquivos/arquivoweb/atendimento/1056 onde 1056 é o id de um Atendimento.

O nome do arquivo segue a seguinte formação: <id Animal>_<id Atendimento>_yyyymmdd_HHmmssSSS.<extensão do arquivo original>
Ex: 241_1051_20170419_022423760.jpg

Desta forma segue um exemplo do caminho completo até um arquivo:
/apache-tomcat-8.0.39/UploadsArquivos/arquivoweb/atendimento/1056/241_1051_20170419_022423760.jpg